import React from 'react'
import {
  CDropdown,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import useTrans from "../../../hooks/useTrans";
import useKeyCloak from "../../../hooks/useKey";

const AppHeaderDropdown = () => {
  const [t, handleClick] = useTrans();

  const [keycloak, isAuthenticated] = useKeyCloak();
  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <CIcon name="cil-user" size="lg"/>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownHeader className="bg-light fw-semibold py-2">Account</CDropdownHeader>

        {(!isAuthenticated) ?

          <CDropdownItem onClick={() => keycloak.login()}>
            Login
          </CDropdownItem> :
          <CDropdownItem onClick={() => keycloak.logout()}>

            Logout {/*({keycloak.tokenParsed.preferred_username})*/}
          </CDropdownItem>
        }
      </CDropdownMenu>
    </CDropdown>
  )
}


export default AppHeaderDropdown
