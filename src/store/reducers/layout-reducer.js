import { SIDEBAR_SHOW, SIDEBAR_UNFOLDABLE } from "../types"

const initialState = {
  sidebarShow: false,
  sidebarUnfoldable: false,
}

export  const layoutReducer =  (state = initialState, action ) => {
  switch (action.type) {
    case SIDEBAR_SHOW:
      return { ...state,
        sidebarShow: action.payload
      }
    case SIDEBAR_UNFOLDABLE:
      return { ...state,
        sidebarUnfoldable: action.payload
      }
    default:
      return state
  }
}
