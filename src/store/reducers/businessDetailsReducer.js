const {ADD_BUSINESSDETAILS_DATA} = require("../types/businessInsurance-types")

const initialBusinessState = {
  formdata: {
    companyDetails_clientCompanyClass: "",
    companyDetails_clientCompanyName: "",
    companyDetails_clientCIN: "",
    companyDetails_clientRegistrationNumber: "",
    companyDetails_clientBusinessDescription: "",

  }
}

const businessDetailsReducer = (state = initialBusinessState, action) =>{
    switch(action.type){
        case ADD_BUSINESSDETAILS_DATA: return {
            ... state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default businessDetailsReducer



