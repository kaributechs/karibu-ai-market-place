const {ADD_INSURANCEQUESTIONS_DATA} = require("../types/personalInsuranceApp-types")

const initialInsuranceQueState = {
    formData:{
        carInsuranceType:"",
        thirdPartyLiability:"",
        bearsAllRisks:"",
        interestedInCourtesyCar:"",
        coversTotalLoss:"",
        coversExcessCosts:"",
        periodOfInsuranceTo:"",
        periodOfInsuranceFrom:""
    }
}

const insuranceQuesFormReducer = (state = initialInsuranceQueState, action) =>{
    switch(action.type){
        case ADD_INSURANCEQUESTIONS_DATA: return {
            ... state,
            formdata: action.payload,
            
        }
        default: return state
    }
}
export default insuranceQuesFormReducer