const { ADD_QUESTIONSFORM_DATA } = require("../types/car-claim-form-types")

const initialQuestionsState = {
    formdata: {
        FinalPageDetails: {
            driversCondition:"",
            accidentHistory:"",
            accidentHistoryDetails:"",
            driversConditionDetails:"",
            vehiclePurpose:"",
            natureOfInjuries:"",
            accidentDescription:"",

        }
    }

}

const questionsFormReducer = (state = initialQuestionsState, action) => {
    switch (action.type) {
        case ADD_QUESTIONSFORM_DATA: return {
            ...state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default questionsFormReducer

