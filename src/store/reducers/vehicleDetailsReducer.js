const {ADD_VEHICLEDETAILS_DATA} = require("../types/businessInsurance-types")

const initialVehicleState = {
  formdata: {
  vehicleDetails_RegistrationNumber: "",
  vehicleDetails_Make: "",
  vehicleDetails_Model: "",
  vehicleDetails_Type: "",
  vehicleDetails_Year: "",
  vehicleDetails_Worth: "",
  vehicleDetails_numberOfSeats: "",
  vehicleDetails_alarmSystemYesOrNo: "",
  vehicleDetails_nightParkingYesOrNo: "",
  vehicleDetails_isVehicleUsedByOthers: "",
  vehicleDetails_Mileage: "",

  }
}

const vehicleDetailsReducer = (state = initialVehicleState, action) =>{
    switch(action.type){
        case ADD_VEHICLEDETAILS_DATA: return {
            ... state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default vehicleDetailsReducer



