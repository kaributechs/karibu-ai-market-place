const {ADD_OTHERDRIVERDETAILS_DATA} = require("../types/businessInsurance-types")

const initialOtherUserState = {
  formdata: {
    otherVehicleUser_Role:"",
    otherVehicleUser_RoleDescription:"",
    otherVehicleUser_MaritalStatus:"",
    otherVehicleUser_FirstName:"",
    otherVehicleUser_LastName:"",
    otherVehicleUser_EmailAddress:"",
    otherVehicleUser_DateOfBirth:"",
    otherVehicleUser_PhoneNumber:"",
    otherVehicleUser_Gender:"",

  }
}

const otherUserDetailsReducer = (state = initialOtherUserState, action) =>{
    switch(action.type){
        case ADD_OTHERDRIVERDETAILS_DATA: return {
            ... state,
            formdata: action.payload,

        }
        default: return state
    }
}
export default otherUserDetailsReducer



