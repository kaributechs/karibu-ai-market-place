import { ADD_CONTACTDETAILS_DATA } from "../types/personalInsuranceApp-types"

export const contactDetailsTask = (formdata) => (dispatch) => {

        dispatch({
          type: ADD_CONTACTDETAILS_DATA,
          payload: formdata,
          
        });

  
}