import {ADD_OTHERDRIVERDETAILS_DATA} from "../types/businessInsurance-types"

export const otherUserFormTask = (formdata) => (dispatch) => {

        dispatch({
          type: ADD_OTHERDRIVERDETAILS_DATA,
          payload: formdata,

        });
  }
