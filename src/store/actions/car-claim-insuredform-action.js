import { ADD_INSUREDFORM_DATA } from "../types/car-claim-form-types"

export const insuredFormTask = (formdata) => (dispatch) => {

        dispatch({
          type: ADD_INSUREDFORM_DATA,
          payload: formdata,
          
        });

  
  }
  
  