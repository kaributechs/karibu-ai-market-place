import React, { useState } from 'react';
import {  Card, CardHeader, CardBody, Col, Form,  FormGroup} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';

import * as Yup from 'yup'


const validationSchema = function () {
  return Yup.object().shape({
    nationalId: Yup.string()
    .matches(/^\(?([0-9]{2})\)?[-. ]?([0-9]{7})([A-Z]{1})[-. ]?([0-9]{2})$/, 'Please Enter A Valid ID Number')
    .min(14, `National ID has to be at least 14 characters`)
    .required('National ID is required'),

  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  nationalId: ""

}

const onSubmit = (values, { setSubmitting }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const DocumentUpload = () =>{

const [selectedFile, setSelectedFile] = useState();
const [isFilePicked, setIsFilePicked] = useState(false);

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };


  
/*
 const  findFirstError =(formName, hasError) => {
    const form = document.forms[formName]
    for (let i = 0; i < form.length; i++) {
      if (hasError(form[i].name)) {
        form[i].focus()
        break
      }
    }
  }

 const  validateForm = (errors)=> {
    findFirstError('simpleForm', (fieldName) => {
      return Boolean(errors[fieldName])
    })
  }


 const  touchAll = (setTouched, errors) =>{
    setTouched({
        nationalId: true,
        clientFirstName: true,
        clientLastName: true,
      
      }
    )
    validateForm(errors)
  }*/
  return(
  
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    
      <Card>
        <CardHeader>
        <strong>Car Insurance Application Form</strong>
        </CardHeader>
        <CardBody>
          <strong>Document Upload</strong>
          <hr />
          <Formik
            initialValues={initialValues}
            validate={validate(validationSchema)}
            onSubmit={onSubmit}
            render={
              ({
                handleSubmit,

              }) => (
                
                    <Form onSubmit={handleSubmit} noValidate name='simpleForm'>

                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <strong>Driver's License</strong>
                    </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <strong>Vehicle Registration Document</strong>
                    </FormGroup>
                    </Col>
                    </FormGroup>


                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
                      <input type="file" name="file" onChange={changeHandler} />
                      {isFilePicked ? (
                        <div>
                          <p>Filename: {selectedFile.name}</p>
                          <p>Filetype: {selectedFile.type}</p>
                          <p>Size in bytes: {selectedFile.size}</p>
                          <p>
                            lastModifiedDate:{" "}
                            {selectedFile.lastModifiedDate.toLocaleDateString()}
                          </p>
                        </div>
                      ) : (
                        <p>Select a file to show details</p>
                      )}
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
                      <input type="file" name="file" onChange={changeHandler} />
                      {isFilePicked ? (
                        <div>
                          <p>Filename: {selectedFile.name}</p>
                          <p>Filetype: {selectedFile.type}</p>
                          <p>Size in bytes: {selectedFile.size}</p>
                          <p>
                            lastModifiedDate:{" "}
                            {selectedFile.lastModifiedDate.toLocaleDateString()}
                          </p>
                        </div>
                      ) : (
                        <p>Select a file to show details</p>
                      )}
                      </FormGroup>
                    </Col>
                    </FormGroup>
                    
                    </Form>
                
              )} />
        </CardBody>
      </Card>
    
    

  </div>
  )
}

export default DocumentUpload;