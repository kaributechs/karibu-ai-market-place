import React from "react";
import {Link} from "react-router-dom"
import UnderwriterTaskTable from "../../../components/tables/UnderwriterTasksTable";

const UnderwriterTaskScreen = () => {
  return (
    <>
     <Link to="/new" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
      <UnderwriterTaskTable/>
    </>
  );
};

export default UnderwriterTaskScreen;
