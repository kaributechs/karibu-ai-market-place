import React from 'react';
import { Button, Card, CardHeader, CardBody, Col, Form, FormFeedback, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom'
import { Formik } from 'formik';

import * as Yup from 'yup'


const validationSchema = function () {
  return Yup.object().shape({
    clientWeight: Yup.string()
    .required('Body Weight Is Required !'),
    clientHeight: Yup.string()
    .required('Height Is Required !'),
    clientSmokes: Yup.string()
    .required('Smokes Is Required !'),
    clientDrinks: Yup.string()
    .required('Drinks Is Required !'),
    clientMedicalCondition: Yup.string()
    .required('Medical Condition Is Required !'),
    clientConditionDes: Yup.string()
    .required('Please Enter Condition If Any !'),
    clientBodyType: Yup.string()
    .required('Body Type Is Required!'),
    clientMedicalReport: Yup.string()
    .required('Please Add Medical Report!'),
    clientCheckUp: Yup.string()
    .required('Please Enter Doctors Checkup Report !'),
    clientFamilyMedicalCondition: Yup.string()
    .required('Client Family Medical Conditions Required !'),
    clientFamilyConditionDes: Yup.string()
    .required('Please Enter Medical Condition If Any !')
  })
}

const validate = (getValidationSchema) => {
  return (values) => {
    const validationSchema = getValidationSchema(values)
    try {
      validationSchema.validateSync(values, { abortEarly: false })
      return {}
    } catch (error) {
      return getErrorsFromValidationError(error)
    }
  }
}

const getErrorsFromValidationError = (validationError) => {
  const FIRST_ERROR = 0
  return validationError.inner.reduce((errors, error) => {
    return {
      ...errors,
      [error.path]: error.errors[FIRST_ERROR],
    }
  }, {})
}

const initialValues = {
  clientWeight: "",
  clientHeight: "",
  clientSmokes: "",
  clientDrinks: "",
  clientConditionDes: "",
  clientMedicalCondition: "",
  clientBodyType:"",
  clientMedicalReport:"",
  clientCheckUp:"",
  clientFamilyMedicalCondition:"",
  clientFamilyConditionDes:""
  
}

const onSubmit = (values, { setSubmitting }) => {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2))
     console.log('User has been successfully saved!', values)
    setSubmitting(false)
  }, 2000)
}



const HealthInsuranceDetailsScreen = () =>{



    /*
     const  findFirstError =(formName, hasError) => {
        const form = document.forms[formName]
        for (let i = 0; i < form.length; i++) {
          if (hasError(form[i].name)) {
            form[i].focus()
            brea          }
        }
      }
        /*
         const  validateForm = (errors)=> {
            findFirstError('simpleForm', (fieldName) => {
              return Boolean(errors[fieldName])
          })
          }

        /*
         const  touchAll = (setTouched, errors) =>{
            setTouched({
                clientWeight: true,
                clientHeight: true,
                clientSmokes: true,
                clientDrinks: true,
                clientConditionDes: true,
                clientMedicalCondition: true,
                clientBodyType: true,
                clientMedicalReport:true,
                clientCheckUp:true,
                clientFamilyMedicalCondition:true,
                clientFamilyConditionDes:true

              }
            )
            validateForm(errors)
          }*/
  return(
    <div className="animated fadeIn">
    <Link to="/new/type" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
    <Card>
      <CardHeader>
       <strong>Complete Medical Insurance Underwriting</strong>
      </CardHeader>
      <CardBody>
       
        <Formik
          initialValues={initialValues}
          validate={validate(validationSchema)}
          onSubmit={onSubmit}
          render={
            ({
              values,
              errors,
              touched,

              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              isValid
            }) => (
              <>
              
                  <Form onSubmit={handleSubmit} noValidate name='simpleForm'>


                  <strong>Personal Medical History</strong>
                  <hr />
                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="12">
                        <FormGroup>
                          <Label for="clientMedicalCondition">Medical Condition</Label>
                          <Input 
                            type="select" 
                            name="clientMedicalCondition" 
                            id="clientMedicalCondition"
                            valid={!errors.clientMedicalCondition}
                            invalid={touched.clientMedicalCondition && !!errors.clientMedicalCondition}
                            required
                            autoFocus={true}
                            onChange={handleChange}
                            onBlur={handleBlur}
                           value={values.clientMedicalCondition} >
                        <option value="">Any Medical Conditions</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </Input>
                          <FormFeedback>{errors.clientMedicalCondition}</FormFeedback>
                        </FormGroup>
                      </Col>
                  </FormGroup>
                  


                  <FormGroup row className="my-0">
                  <Col  xs="12" sm="6"  lg="12">
                        <FormGroup>
                          <Label for="clientConditionDes">Explain Condition</Label>
                          <Input type="textarea"
                                 name="clientConditionDes"
                                 id="clientConditionDes"
                                 placeholder="Explain Conditions If Any"
                                 autoComplete="clientConditionDes"
                                 valid={!errors.clientConditionDes}
                                 invalid={touched.clientConditionDes && !!errors.clientConditionDes}
                                 required
                                 onChange={handleChange}
                                 onBlur={handleBlur}
                                 value={values.clientConditionDes} />
                          <FormFeedback>{errors.clientConditionDes}</FormFeedback>
                        </FormGroup>
                      </Col>

                    <Col xs="12" sm="6"  lg="4">
                    <FormGroup>
                    <Label for="clientMedicalReport">Medical Report</Label>
                      <Input type="file"
                             name="clientMedicalReport"
                             id="clientMedicalReport"
                             placeholder="Attach Medical Report"
                             autoComplete="clientMedicalReport"
                             valid={!errors.clientMedicalReport}
                             invalid={touched.clientMedicalReport && !!errors.clientMedicalReport}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientMedicalReport} />
                    <FormFeedback>{errors.clientMedicalReport}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col xs="12" sm="6"  lg="4">
                    <FormGroup>
                    <Label for="clientMedicalReport">Doctors Checkup</Label>
                      <Input type="file"
                             name="clientCheckUp"
                             id="clientCheckUp"
                             placeholder="Doctors Checkup"
                             autoComplete="clientCheckUp"
                             valid={!errors.clientCheckUp}
                             invalid={touched.clientCheckUp && !!errors.clientCheckUp}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientCheckUp} />
                    <FormFeedback>{errors.clientCheckUp}</FormFeedback>
                    </FormGroup>
                    </Col>

                  </FormGroup>
                    
                  <strong>Family Medical History</strong>
                  <hr/>

                  <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="12">
                    <FormGroup>
                    <Label for="clientFamilyMedicalCondition">Any Family Medical Conditions</Label>
                    <Input 
                            type="select" 
                            name="clientFamilyMedicalCondition" 
                            id="clientFamilyMedicalCondition"
                            valid={!errors.clientFamilyMedicalCondition}
                            invalid={touched.clientFamilyMedicalCondition && !!errors.clientFamilyMedicalCondition}
                            required
                            onChange={handleChange}
                            onBlur={handleBlur}
                           value={values.clientFamilyMedicalCondition} >
                        <option value="">Any Medical Conditions</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </Input>
                    <FormFeedback>{errors.clientFamilyMedicalCondition}</FormFeedback>
                    </FormGroup>
                    </Col>
                  </FormGroup>

                  <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="12">
                    <FormGroup>
                    <Label for="clientFamilyConditionDes">Explain If Any</Label>
                      <Input type="textarea"
                             name="clientFamilyConditionDes"
                             id="clientFamilyConditionDes"
                             placeholder="Explain If Any"
                             autoComplete="clientFamilyConditionDes"
                             valid={!errors.clientFamilyConditionDes}
                             invalid={touched.clientFamilyConditionDes && !!errors.clientFamilyConditionDes}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientFamilyConditionDes} />
                    <FormFeedback>{errors.clientFamilyConditionDes}</FormFeedback>

                    </FormGroup>
                    </Col>
                  </FormGroup>

                  <strong>Build</strong>
                  <hr />
                    <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="4">
                    <FormGroup>
                      <Label for="clientWeight">Weight</Label>
                      <Input type="text"
                             name="clientWeight"
                             id="clientWeight"
                             placeholder="Weight (kg)"
                             autoComplete="clientWeight"
                             valid={!errors.clientWeight}
                             invalid={touched.clientWeight && !!errors.clientWeight}
                             autoFocus={false}
                             required
                             onChange={handleChange}
                             onBlur={handleBlur}
                             value={values.clientWeight} />
                    <FormFeedback>{errors.clientWeight}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="4">
                      <FormGroup>
                        <Label for="clientHeight">Height</Label>
                        <Input type="text"
                              name="clientHeight"
                              id="clientHeight"
                              placeholder="Height (cm)"
                              autoComplete="clientHeight"
                              valid={!errors.clientHeight}
                              invalid={touched.clientHeight && !!errors.clientHeight}
                              required
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.clientHeight} />
                        <FormFeedback>{errors.clientHeight}</FormFeedback>
                      </FormGroup>
                      </Col>
                      
                  <Col  xs="12" sm="6"  lg="4">
                  <FormGroup>
                      <Label htmlFor="clientBodyType">Body Type</Label>
                      <Input 
                       type="select" 
                       name="clientBodyType" 
                       id="clientBodyType"
                       valid={!errors.clientBodyType}
                       invalid={touched.clientBodyType && !!errors.clientBodyType}
                       required
                       onChange={handleChange}
                       onBlur={handleBlur}
                       value={values.clientBodyType} >
                         <option value="">Select Body Type</option>
                        <option value="ectomorph">Ectomorph</option>
                        <option value="mesomorph">Mesomorph</option>
                        <option value="endomorph">Endomorph</option>
                      </Input>
                      <FormFeedback>{errors.clientBodyType}</FormFeedback>
                    </FormGroup>
                  </Col>
                </FormGroup>



                  <strong>Habits</strong>
                  <hr />

                  <FormGroup  row className="my-0">
                    <Col  xs="12" sm="6"  lg="4">
                    <FormGroup>
                      <Label for="clientSmokes">Smokes</Label>
                      <Input 
                         type="select" 
                         name="clientSmokes" 
                         id="clientSmokes"
                         valid={!errors.clientSmokes}
                         invalid={touched.clientSmokes && !!errors.clientSmokes}
                         required
                         onChange={handleChange}
                         onBlur={handleBlur}
                         value={values.clientSmokes} >
                        <option value="">Client Smokes</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </Input>
                      <FormFeedback>{errors.clientSmokes}</FormFeedback>
                    </FormGroup>
                    </Col>

                    <Col  xs="12" sm="6"  lg="4">
                      <FormGroup>
                      <Label for="clientDrinks">Drinks</Label>
                      <Input 
                         type="select" 
                         name="clientDrinks" 
                         id="clientDrinks"
                         valid={!errors.clientDrinks}
                         invalid={touched.clientDrinks && !!errors.clientDrinks}
                         required
                         onChange={handleChange}
                         onBlur={handleBlur}
                         value={values.cclientDrinks} >
                        <option value="">Client Drinks</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </Input>
                      <FormFeedback>{errors.clientDrinks}</FormFeedback>
                    </FormGroup>
                    </Col>
                  </FormGroup>

                 

                    <FormGroup>
                      <Button type="submit" color="success" className="mr-1" disabled={isSubmitting || !isValid}>{isSubmitting ? 'Wait...' : 'Complete Application'}</Button>
                    </FormGroup>
                  </Form>
              


               
              </>
            )} />
      </CardBody>
    </Card>
  </div>
  )
}






export default HealthInsuranceDetailsScreen;
