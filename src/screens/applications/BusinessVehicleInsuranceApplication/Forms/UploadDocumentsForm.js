import React, {useState} from 'react';
import { Col,  FormGroup, Label} from 'reactstrap';
import {Link} from "react-router-dom";
import {CountryDropdown, RegionDropdown} from 'react-country-region-selector';
import {useFormikContext} from 'formik';
import { InputField, InputSelect} from "../../FormFields";
import {CFormLabel} from "@coreui/react";

export const UploadDocumentsForm = () => {

    const [selectedVehicleRegFile, setSelectedVehicleRegFile] = useState();
    const [isVehicleRegFilePicked, setIsVehicleRegFilePicked] = useState(false);

    const [selectedDriverLicenseFile, setSelectedDriverLicenseFile] = useState();
    const [isDriverLicenseFilePicked, setIsDriverLicenseFilePicked] = useState(false);


    const driverLicensechangeHandler = (event) => {
        setSelectedDriverLicenseFile(event.target.files[0]);
        setIsDriverLicenseFilePicked(true);
    };


    const vehicleRegChangeHandler = (event) => {
        setSelectedVehicleRegFile(event.target.files[0]);
        setIsVehicleRegFilePicked(true);
    };


    return (
        <div className="animated fadeIn">

            <strong>DocumentUpload</strong>
            <hr/>

            <FormGroup row className="my-0">
                <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <strong>Driver's License</strong>
                    </FormGroup>
                </Col>
                      
                <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                        <strong>Vehicle Registration Document</strong>
                    </FormGroup>
                </Col>
            </FormGroup>


            <FormGroup row className="my-0">
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
                      <input type="file" name="file" onChange={driverLicensechangeHandler} />
                      {isDriverLicenseFilePicked ? (
                        <div>
                          <p>Filename: {selectedDriverLicenseFile.name}</p>
                          <p>Filetype: {selectedDriverLicenseFile.type}</p>
                          <p>Size in bytes: {selectedDriverLicenseFile.size}</p>
                          <p>
                            lastModifiedDate:{" "}
                            {selectedDriverLicenseFile.lastModifiedDate.toLocaleDateString()}
                          </p>
                        </div>
                      ) : (
                        <p>Select a file to show details</p>
                      )}
                      </FormGroup>
                    </Col>
                      
                    <Col  xs="12" sm="6"  lg="6">
                    <FormGroup>
                      <Link to="/claims" className="mb-2"><span className="material-icons">keyboard_backspace</span></Link>
                      <input type="file" name="file" onChange={vehicleRegChangeHandler} />
                      {isVehicleRegFilePicked ? (
                        <div>
                          <p>Filename: {selectedVehicleRegFile.name}</p>
                          <p>Filetype: {selectedVehicleRegFile.type}</p>
                          <p>Size in bytes: {selectedVehicleRegFile.size}</p>
                          <p>
                            lastModifiedDate:{" "}
                            {selectedVehicleRegFile.lastModifiedDate.toLocaleDateString()}
                          </p>
                        </div>
                      ) : (
                        <p>Select a file to show details</p>
                      )}
                      </FormGroup>
                    </Col>
                    </FormGroup>

        </div>
    )
}
