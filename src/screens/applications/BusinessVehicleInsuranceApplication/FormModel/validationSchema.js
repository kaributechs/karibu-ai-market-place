import * as Yup from 'yup';
import applicationDetailFormModel from "./applicationDetailFormModel";
const {
  formField: {


     VehicleUserDetails: {
    vehicleUserDetails_LastName,
    vehicleUserDetails_FirstName,
    vehicleUserDetails_LicenseNumber,
    vehicleUserDetails_NationalId,
    vehicleUserDetails_DateOfBirth,
    vehicleUserDetails_EmailAddress,
    vehicleUserDetails_PhoneNumber,
    vehicleUserDetails_Gender,
    vehicleUserDetails_Role,
    vehicleUserDetails_RoleDescription,
    vehicleUserDetails_MaritalStatus,
},

    CompanyDetails: {
    companyDetails_clientCompanyClass,
    companyDetails_clientCompanyName,
    companyDetails_clientCIN,
    companyDetails_clientRegistrationNumber,
    companyDetails_clientBusinessDescription,
},

     ContactDetails: {
    contactDetails_clientEmailAddress,
    contactDetails_clientPhoneNumber,
    contactDetails_clientResidentialStreetAddress,
    contactDetails_clientResidentialCity,
    contactDetails_clientRegion,
    contactDetails_residentialZip,
    contactDetails_workAddressZip,
    contactDetails_clientCountry,
    contactDetails_clientWorkStreetAddress,
    contactDetails_clientWorkCity,
    contactDetails_clientZip,
    contactDetails_workRegion,
    contactDetails_workCountry,
},

    VehicleDetails: {
    vehicleDetails_RegistrationNumber,
    vehicleDetails_Make,
    vehicleDetails_Model,
    vehicleDetails_Year,
    vehicleDetails_Type,
    vehicleDetails_Mileage,
    vehicleDetails_Worth,
    vehicleDetails_Usage,
    vehicleDetails_numberOfSeats,
    vehicleDetails_alarmSystemYesOrNo,
    vehicleDetails_nightParkingYesOrNo,
    vehicleDetails_isVehicleUsedByOthers,
},

    OtherVehicleUser: {
    otherVehicleUser_LicenseNumber,
    otherVehicleUser_NationalId,
    otherVehicleUser_FirstName,
    otherVehicleUser_LastName,
    otherVehicleUser_DateOfBirth,
    otherVehicleUser_EmailAddress,
    otherVehicleUser_PhoneNumber,
    otherVehicleUser_Gender,
    otherVehicleUser_MaritalStatus,
    otherVehicleUser_Role,
    otherVehicleUser_RoleDescription,
},

    InsuranceQuestions: {
    insuranceQuestions_carInsuranceType,
    insuranceQuestions_thirdPartyLiability,
    insuranceQuestions_allRisksYesOrNo,
    insuranceQuestions_replacementCarYesOrNo,
    insuranceQuestions_totalLossYesOrNo,
    insuranceQuestions_excessYesOrNo,
    insuranceQuestions_periodOfInsuranceTo,
    insuranceQuestions_periodOfInsuranceFrom,
    maritalStatus,
}
  }
} = applicationDetailFormModel;


export default [
  Yup.object().shape({
    [companyDetails_clientCompanyName.name]: Yup.string()
      .required('Company Name is required')
      .min(2, `Company Name has to be at least 2 characters`),
    [companyDetails_clientCompanyClass.name]: Yup.string()
      .required('Company Class is required'),
    [companyDetails_clientCIN.name]: Yup.string()
      .min(21, "CIN has to be at least 21 characters")
      .required('CIN is required'),
    [companyDetails_clientRegistrationNumber.name]: Yup.string()
      .min(8, `Registration Number  has to be at least 8 characters `)
      .required('Company Registration Number is required'),
    [companyDetails_clientBusinessDescription.name]: Yup.string()
      .required('Business Description is required'),
  }),

  Yup.object().shape({
    [contactDetails_clientEmailAddress.name]: Yup.string()
      .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, 'Please Enter Valid Email Address')
      .email('Invalid email address ')
      .required('Email address is required!'),
    [contactDetails_clientPhoneNumber.name]: Yup.string()
      .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/, 'Please Enter Valid Phone NUmber')
      .max(10, 'Phone number should be at least 10 characters')
      .required('Phone Number is required'),
    [contactDetails_clientResidentialStreetAddress.name]: Yup.string()
      .required('Address Line One Required!'),
    [contactDetails_clientResidentialCity.name]: Yup.string()
      .required('Address Line Two Required!'),
    [contactDetails_clientRegion.name]: Yup.string()
      .required('Region is Required!'),
    [contactDetails_residentialZip.name]: Yup.string()
      .required('Zip is Required!')
      .matches (/(^\d{4}$)|(^\d{4}-\d{4}$)/, 'Please Enter Valid Zip Code'),
    [contactDetails_clientCountry.name]: Yup.string()
      .required('Client Country is Required!'),
    [contactDetails_clientWorkStreetAddress.name]: Yup.string()
      .required('Address Line One Required!'),
    [contactDetails_clientWorkCity.name]: Yup.string()
      .required('Address Line Two Required!'),
    [contactDetails_workAddressZip.name]: Yup.string()
      .matches (/(^\d{4}$)|(^\d{4}-\d{4}$)/, 'Please Enter Valid Zip Code')
      .required('Zip is Required!'),

  }),

  Yup.object().shape({
    [vehicleUserDetails_LastName.name]: Yup.string()
      .required('First Name is required')
      .min(2, `First Name has to be at least 2 characters`),
    [vehicleUserDetails_FirstName.name]: Yup.string()
      .required('Last Name is required')
      .min(2, `Last Name has to be at least 2 characters`),
    [vehicleUserDetails_LicenseNumber.name]: Yup.string()
      .required('License Number is required')
      .min(16, `License Number has to be at least 16 characters`),
    [vehicleUserDetails_NationalId.name]: Yup.string()
      .required('ID Number is required')
      .min(8, `License Number has to be at least 8 characters`),
    [vehicleUserDetails_DateOfBirth.name]: Yup.string()
      .required('Date of Birth is required'),
    [vehicleUserDetails_EmailAddress.name]: Yup.string()
      .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$/, 'Please Enter Valid Email Address')
      .email('Invalid email address ')
      .required('Email address is required!'),
    [vehicleUserDetails_PhoneNumber.name]: Yup.string()
      .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/, 'Please Enter Valid Phone NUmber')
      .max(10, 'Phone number should be at least 10 characters')
      .required('Phone Number is required'),
    [vehicleUserDetails_Gender.name]: Yup.string()
      .required('Gender is required'),
    [vehicleUserDetails_MaritalStatus.name]: Yup.string()
      .required('Marital Status is required'),

  }),

  Yup.object().shape({
    [vehicleDetails_RegistrationNumber.name]: Yup.string()
      .required('Vehicle Registration Number is required'),
    [vehicleDetails_Make.name]: Yup.string()
      .required('Vehicle Make is required'),
    [vehicleDetails_Model.name]: Yup.string()
      .required('Vehicle Model is required'),
    [vehicleDetails_Type.name]: Yup.string()
      .required('Vehicle Type is required'),
    [vehicleDetails_Worth.name]: Yup.string()
      .required('Vehicle Price is required!'),
    [vehicleDetails_alarmSystemYesOrNo.name]: Yup.string()
      .required('This field is required'),
    [vehicleDetails_nightParkingYesOrNo.name]: Yup.string()
      .required('This field is required'),
    [vehicleDetails_isVehicleUsedByOthers.name]: Yup.string()
      .required('This field is required'),
    [vehicleDetails_Mileage.name]: Yup.string()
      .required('Vehicle Mileage is required'),
    [vehicleDetails_numberOfSeats.name]: Yup.string()
      .required('Number of Seats is required'),

  }),

  Yup.object().shape({
    [otherVehicleUser_LastName.name]: Yup.string()
      .required('First Name is required')
      .min(2, `First Name has to be at least 2 characters`),
    [otherVehicleUser_FirstName.name]: Yup.string()
      .required('Last Name is required')
      .min(2, `Last Name has to be at least 2 characters`),
    [otherVehicleUser_LicenseNumber.name]: Yup.string()
      .required('License Number is required')
      .min(16, `License Number has to be at least 16 characters`),
    [otherVehicleUser_NationalId.name]: Yup.string()
      .required('ID Number is required')
      .min(8, `License Number has to be at least 8 characters`),
    [otherVehicleUser_DateOfBirth.name]: Yup.string()
      .required('Date of Birth is required'),
    [otherVehicleUser_EmailAddress.name]: Yup.string()
      .matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$/, 'Please Enter Valid Email Address')
      .email('Invalid email address ')
      .required('Email address is required!'),
    [otherVehicleUser_PhoneNumber.name]: Yup.string()
      .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/, 'Please Enter Valid Phone NUmber')
      .max(10, 'Phone number should be at least 10 characters')
      .required('Phone Number is required'),
    [otherVehicleUser_MaritalStatus.name]: Yup.string()
      .required('Marital Status is required'),
    [otherVehicleUser_Gender.name]: Yup.string()
      .required('Gender is required'),


  }),


  Yup.object().shape({
   [insuranceQuestions_carInsuranceType.name]: Yup.string()
      .required('Car Insurance Type is required'),
    [insuranceQuestions_thirdPartyLiability.name]: Yup.string()
      .required('Third Party Liability is required'),
    [insuranceQuestions_allRisksYesOrNo.name]: Yup.string()
      .required('This Field is required'),
    [insuranceQuestions_replacementCarYesOrNo.name]: Yup.string()
      .required('This Field is required'),
    [insuranceQuestions_periodOfInsuranceFrom.name]: Yup.string()
      .required('From Period is required'),
    [insuranceQuestions_periodOfInsuranceTo.name]: Yup.string()
      .required('To Period is required'),
  }),



];

