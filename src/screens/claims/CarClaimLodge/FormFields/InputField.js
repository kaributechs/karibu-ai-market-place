import React from 'react';
import {useField, Field} from 'formik';
import {CFormControl, CFormLabel} from "@coreui/react";
import {FormFeedback, FormGroup} from "reactstrap";

export const InputField = (props) => {
  const {errorText, name, label, ...rest} = props;
  const [field, meta] = useField(props);

  return (
    <FormGroup>
      <CFormLabel htmlFor={name}>{label}</CFormLabel>
      <CFormControl
        invalid={(meta.touched || meta.submitCount)&& meta.error && true}
        valid={ meta.touched && !meta.error}
        {...field}
        {...rest}
      />
      <FormFeedback>{meta.error}</FormFeedback>
    </FormGroup>

  );
}
