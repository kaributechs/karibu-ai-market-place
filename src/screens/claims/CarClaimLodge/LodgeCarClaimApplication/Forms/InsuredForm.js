import React, { useState } from 'react';
import { Col, FormGroup, Label, Input } from 'reactstrap';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
import { useFormikContext } from 'formik';
import { InputField } from "../../FormFields";
import { CFormLabel } from "@coreui/react";
import { connect } from 'react-redux';
import { Provider } from 'react-redux';
import store from 'src/store';
import { useSelector, useDispatch } from 'react-redux'
import { InputSelect } from 'src/screens/applications/FormFields';

export const InsuredForm = (props) => {

  const dispatch = useDispatch()
  const insuredObj = useSelector((state) => state.insured?.formdata)

  const salutations = [
    {text: "Select your Salutation", value:""},
      {text: "Mr", value: "Mr"},
      {text: "Mrs", value: "Mrs"},
      {text: "Miss", value: "Miss"},
      {text: "Ms", value: "Ms"},
      {text: "Other", value: "Other"},
  ]

  const {
    formField: {
      screenId,
      InsuredDetails: {  
        insuredId,
        insuredSalutation,
        insuredFirstName,
        insuredLastName,
        insuredEmail,
        insuredPhoneNumber,
        insuredResidentialStreetAddress,
        insuredSurbub,
        insuredResidentialCity,
        insuredRegion,
        insuredCountry,
        insuredZipCode,
        insuredWorkingStreetAddress,
        insuredWorkingSurbub,
        insuredWorkingCity,
        insuredWorkingRegion,
        insuredWorkingCountry,
        insuredWorkingZipCode
      }


    }
  } = props;


  //TODO: fix form field for country and region
  const { values: formValues } = useFormikContext();

  const [countryDll, setCountryDll] = useState(formValues.insuredCountry)
  const [regionDll, setRegionDll] = useState(formValues.insuredRegion)
  const [workCountryDll, setWorkCountryDll] = useState(formValues.insuredWorkingCountry)
  const [workRegionDll, setWorkRegionDll] = useState(formValues.insuredWorkingRegion)


  const handleCountryChange = (country) => {
    formValues.insuredCountry = country;
    setCountryDll(country);
  }

  const handleRegionChange = (region) => {
    formValues.insuredRegion = region;
    setRegionDll(region);
  }

  const handleWorkCountryChange = (workCountry) => {
    formValues.insuredWorkingCountry = workCountry;
    setWorkCountryDll(workCountry);
  }

  const handleWorkRegionChange = (workRegion) => {
    formValues.workRegion = workRegion;
    setWorkRegionDll(workRegion);
  }

screenId.name = 'Insured_Form_Page'


  return (

    <div className="animated fadeIn">

      <FormGroup row className="my-0">
        <span style={{ paddingTop: "15px" }}></span>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>

            <InputField type="text"

              name={insuredId.name}
              id={insuredId.name}
              label={<Label htmlFor="insuredId"><span style={{ color: "red" }}>*</span>National ID</Label>}
              autoComplete="National ID"
              placeholder="National ID">
              </InputField>
          </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputSelect data= {salutations} 
              name={insuredSalutation.name}
              id={insuredSalutation.name}
              label = {<Label htmlFor="salutations"><span style={{color:"red", size:12}}>*</span>Salutation</Label>}
               value="value" text="text">
              
            </InputSelect>

          </FormGroup>

        </Col>

      </FormGroup>

      <FormGroup row className="my-0">
        <span style={{ paddingTop: "5px" }}></span>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>

            <InputField type="text"

              name={insuredFirstName.name}
              id={insuredFirstName.name}
              label={<Label htmlFor="insuredFirstName"><span style={{ color: "red" }}>*</span>First Name</Label>}
              autoComplete="FirstName"
              placeholder="First Name" />
          </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="text"
              name={insuredLastName.name}
              id={insuredLastName.name}
              label={<Label htmlFor="insuredLastName"><span style={{ color: "red" }}>*</span>Last Name</Label>}

              placeholder="Last Name" />
          </FormGroup>

        </Col>

      </FormGroup>



      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="email"
              name={insuredEmail.name}
              id={insuredEmail.name}
              label={<Label htmlFor="insuredEmail"><span style={{ color: "red" }}>*</span>Email</Label>}

              autoComplete="Email"
              placeholder="Email Address" /> </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="text"
              name={insuredPhoneNumber.name}
              id={insuredPhoneNumber.name}
              label={<Label htmlFor="phoneNumber"><span style={{ color: "red" }}>*</span>Phone Number</Label>}

              placeholder="Phone Number" /> </FormGroup>

        </Col>
      </FormGroup>

      <hr />

      <FormGroup row className="my-0">

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <strong>Residential Address</strong>
          </FormGroup>
        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <strong>Work Address</strong>
          </FormGroup>
        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="textarea"
              rows={2}
              name={insuredResidentialStreetAddress.name}
              id={insuredResidentialStreetAddress.name}
              label={<Label htmlFor="insuredResidentialStreetAddress"><span style={{ color: "red" }}>*</span>Street Address</Label>}

              placeholder="Street Address" /> </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="textarea"
              rows={2}
              name={insuredWorkingStreetAddress.name}
              id={insuredWorkingStreetAddress.name}
              label={<Label htmlFor="insuredWorkingStreetAddress"><span style={{ color: "red" }}>*</span>Working Street Address</Label>}

              placeholder="Working Street Address" /> </FormGroup>

        </Col>
      </FormGroup>
       
       <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="textarea"
              rows={2}
              name={insuredSurbub.name}
              id={insuredSurbub.name}
              label={<Label htmlFor="insuredResidentialStreetAddress"><span style={{ color: "red" }}>*</span>Surbub</Label>}

              placeholder="Surbub" /> </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="textarea"
              rows={2}
              name={insuredWorkingSurbub.name}
              id={insuredWorkingSurbub.name}
              label={<Label htmlFor="insuredWorkingSurbub"><span style={{ color: "red" }}>*</span>Working Surbub</Label>}

              placeholder="Working Surbub" /> </FormGroup>

        </Col>
      </FormGroup>

      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <FormGroup>

            <InputField type="text"
              name={insuredResidentialCity.name}
              id={insuredResidentialCity.name}
              placeholder="City"
              label={<Label htmlFor="insuredResidentialCity"><span style={{ color: "red" }}>*</span>City</Label>}

            />

          </FormGroup>



        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="text"
              name={insuredWorkingCity.name}
              id={insuredWorkingCity.name}
              placeholder="Working City"
              label={<Label htmlFor="insuredResidentialCity"><span style={{ color: "red" }}>*</span>Working City</Label>}
            />

          </FormGroup>


        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <span style={{ paddingTop: '15px' }}></span>
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <CFormLabel for={insuredCountry.name}><span style={{ color: "red" }}>*</span>Country</CFormLabel>
            <div style={{ borderColor: 'green' }}>
              <CountryDropdown
                name={insuredCountry.name}
                className=" form-control mb-3"
                id={insuredCountry.name}
                value={countryDll}
                onChange={handleCountryChange} />
            </div>
          </FormGroup>

        </Col>

        <Col xs="12" sm="6" lg="6">

          <FormGroup>
            <CFormLabel for={insuredWorkingCountry.name}><span style={{ color: "red" }}>*</span>Working Country</CFormLabel>
            <div style={{ borderColor: 'green' }}>
              <CountryDropdown
                className=" form-control mb-3"
                name={insuredWorkingCountry.name}
                id={insuredWorkingCountry.name}
                value={workCountryDll}
                onChange={handleWorkCountryChange} />
            </div></FormGroup>



        </Col>
      </FormGroup>


      <FormGroup row className="my-0">
        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <CFormLabel for={insuredRegion.name}><span style={{ color: "red" }}>*</span>Region</CFormLabel>
            <RegionDropdown
              country={formValues.insuredCountry}
              className=" form-control mb-3"
              name={insuredRegion.name}
              id={insuredRegion.name}

              value={regionDll}
              onChange={handleRegionChange} /></FormGroup>




        </Col>

        <Col xs="12" sm="6" lg="6">

          <FormGroup>
            <CFormLabel for={insuredWorkingRegion.name}><span style={{ color: "red" }}>*</span>Working Region</CFormLabel>
            <RegionDropdown
              country={formValues.insuredWorkingCountry}
              className=" form-control mb-3"
              name={insuredWorkingRegion.name}
              id={insuredWorkingRegion.name}

              value={workRegionDll}
              onChange={handleWorkRegionChange} /></FormGroup>


        </Col>
      </FormGroup>

      <FormGroup row className="my-0">

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="number"
              name={insuredZipCode.name}
              id={insuredZipCode.name}
              label={<Label htmlFor="insuredWorkingipCode"><span style={{ color: "red" }}>*</span>Zip Code</Label>}
            />

          </FormGroup>



        </Col>

        <Col xs="12" sm="6" lg="6">
          <FormGroup>
            <InputField type="number"
              name={insuredWorkingZipCode.name}
              id={insuredWorkingZipCode.name}
              label={<Label htmlFor="insuredWorkingZipCode"><span style={{ color: "red" }}>*</span>Working Zip Code</Label>}
            />

          </FormGroup>


        </Col>
      </FormGroup>


    </div>
  )
}

