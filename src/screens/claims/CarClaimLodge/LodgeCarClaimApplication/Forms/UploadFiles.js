import React, { useState } from "react";
import axios from "axios";
import { CFormLabel } from "@coreui/react";
import { Col, FormGroup, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom'

function UploadFiles() {
 // const [lastModifiedDate, setLastModifiedDate] = useState("") ;
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  const [selectedFile1, setSelectedFile1] = useState();
  const [isFilePicked1, setIsFilePicked1] = useState(false);



  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };

  const changeHandler1 = (event) => {
    setSelectedFile1(event.target.files[0]);
    setIsFilePicked1(true);
  };



  const handleSubmission = () => {
    
    const formData = new FormData();

    formData.append("files", selectedFile,selectedFile1);


    const uploadFile = () =>{
        axios.post(`/api/v1/files/upload`,  
            formData
        ).then(()=>{
          console.log("**Sucesss*")
        }).catch(()=>{
          console.log("***Errors**")
        })
      

    }

    uploadFile();

  };

  return (
    <div >
      <FormGroup row className="my-0">

      <span style = {{paddingTop:'10px'}}></span>
      <Col xs="12" sm="6" lg="6">
      <FormGroup>
      <CFormLabel for='policeReport'><span style={{textDecorationLine: 'underline',fontSize:"17px"}}>Police Report</span></CFormLabel>
      <input type="file" name="file" onChange={changeHandler} />
      {isFilePicked ? (
        <div>
          <p>Filename: {selectedFile.name}</p>
          <p>Filetype: {selectedFile.type}</p>
          <p>Size in bytes: {selectedFile.size}</p>
          
        </div> 
      ) : (
        <p>Select a file to show details</p>
      )}
      
      </FormGroup>
      </Col>

      <span style = {{paddingBlock:'10px'}}></span>
      <Col xs="12" sm="6" lg="6">
      <FormGroup>
      <CFormLabel for='additionalDocuments'><span style={{textDecorationLine: 'underline',fontSize:"17px"}}>Additional Documents</span></CFormLabel>
      <input type="file" name="file" onChange={changeHandler1} />
      {isFilePicked1 ? (
        <div>
          <p>Filename: {selectedFile1.name}</p>
          <p>Filetype: {selectedFile1.type}</p>
          <p>Size in bytes: {selectedFile1.size}</p>
          
        </div> 
      ) : (
        <p>Select a file to show details</p>
      )}
      
      </FormGroup>
      </Col>

      

      </FormGroup>
    </div>
  );
}

export default UploadFiles;


