import React from 'react';
import {Card, CardHeader, CardBody} from 'reactstrap';


const AvailableTasksTable =({tasks})=>{
    const table = tasks;
      const columns = [
      {
        dataField: "id",
        text: "Id",
      },
      {
        dataField: "name",
        text: "Name",
      },

      {
        dataField: "assignee",
        text: "Assignee",
      },
    ];

    return (
      <div className="animated">
        <Card>
          <CardHeader>
            Available Tasks
          </CardHeader>
          <CardBody >
            Table here
          </CardBody>
        </Card>
      </div>
    );

}

export default AvailableTasksTable;
