import React from 'react'
import { Link } from "react-router-dom"
import {Table} from "reactstrap"
import { useKeycloak } from '@react-keycloak/web'

const ApproveTasksTable = ({tasks, handleApproveTask, handleRejectedTask, handleMoreDetails}) => {

  const { keycloak, initialized } = useKeycloak()

  const appManager  = keycloak.hasRealmRole('app-manager')

  const appConsultant  = keycloak.hasRealmRole('app-consultant')

  const appCustomer = keycloak.hasRealmRole('app-customer')
  
  console.log(appManager)


    return (
        <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>STATUS</th>
            <th>NAME</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
  
          { (tasks.length > 0) ? tasks.map( (tk, index) => {
           return (
            <tr key={ index }>
              <td>{ tk.id }</td>
              <td>{ tk.name }</td>
              <td>{ tk.assignee}</td>
              <td>
              {tk.id  && appManager ?
                <button className="btn btn-success"  onClick={()=>handleApproveTask(tk)}>Approve</button>: null
                }
                
              { tk.id  && appManager ?
                <button className="btn btn-danger ml-2"  onClick={()=>handleRejectedTask(tk)}>Reject</button>: null
              }
              { tk.id ?
                 <Link to={`/claims/adminster/view-evidence/${tk.id}`} className="btn btn-warning ml-2">Evidence</Link>: null
              }
              {
                tk.name && appManager || appCustomer?
                <Link className="btn btn-success ml-2"  to={`/claims/adminster/more-details/${tk.processInstanceId}`}>Details</Link>: null
              }
              {
                tk.name  && appManager?
                <Link className="btn btn-danger ml-2"  to={`/claims/adminster/handle-assessment/${tk.id}`}>Assessment</Link>: null
              }

              {
                tk.name &&  appManager?
                <Link className="btn btn-success ml-2"  to={`/claims/adminster/inspect-vehicle/${tk.id}`}>Inspect</Link>: null
              }

            {tk.id  && appCustomer || appConsultant?
                <button className="btn btn-success ml-2"  onClick={()=>handleApproveTask(tk)}>Complete</button>: null
                } 

              </td>
            </tr>
          )
         }) : <tr><td colSpan="5"><h1>No Tasks Yet</h1></td></tr> }
        </tbody>
      </Table>
    )
}

export default ApproveTasksTable
