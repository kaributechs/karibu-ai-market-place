import React from 'react'
import CIcon from '@coreui/icons-react'
import { NavLink } from 'react-router-dom'

const _nav = [
  {
    _component: 'CNavItem',
    as: NavLink,
    anchor: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="nav-icon" />,
    badge: {
      color: 'info',
      text: 'NEW',
    },
  },

  {
    _component: 'CNavGroup',
    as: NavLink,
    anchor: 'Claims Management',
    to: '/claims',
    icon: <CIcon name="cil-puzzle" customClasses="nav-icon" />,
    items: [
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Claims',
        to: '/claims',
      },
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Lodge Claim',
        to: '/lodge',
      },
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Track Claim',
        to: '/track',
      },
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Cancel Claim',
        to: '/cancel',
      }
     ],
  },
  {
    _component: 'CNavGroup',
    anchor: 'Dealer',
    icon: <CIcon name="cil-cursor" customClasses="nav-icon" />,
    items: [
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Find A Dealer',
        to: '/find',
      }
    ],
  },
  {
    _component: 'CNavGroup',
    anchor: 'Documentation',
    icon: <CIcon name="cil-notes" customClasses="nav-icon" />,
    items: [
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Submit Documentation',
        to: '/submit',
      }
    ],
  },
  {
    _component: 'CNavGroup',
    anchor: 'Account',
    icon: <CIcon name="cil-star" customClasses="nav-icon" />,
    items: [
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Login',
        to: '/login',
      },
      {
        _component: 'CNavItem',
        as: NavLink,
        anchor: 'Register',
        to: '/register',
      }
    ],
  },
]

export default _nav
